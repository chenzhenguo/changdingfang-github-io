#!/bin/bash
# =======================================================================
#  FileName:     wget.sh
#  Author:       dingfang
#  CreateDate:   2021-02-23 17:29:23
#  ModifyAuthor: dingfang
#  ModifyDate:   2021-02-23 17:32:30
# =======================================================================

url=http://maxogden.github.io/slides/nodepdx/images

wget $url/yellow.png
wget $url/blogpost.png
wget $url/crittercrreator.png
wget $url/voxeljscom.png
wget $url/copy.png
wget $url/modulearticle.png
wget $url/blockplot.png
wget $url/voxelcreator.png
wget $url/three.png
wget $url/npm.png
wget $url/node.png
wget $url/browserify.png
wget $url/beefy.png
wget $url/gifblocks.png
wget $url/drone.png
wget $url/craft.png
wget $url/skin.png
wget $url/archiveroom.png
wget $url/nyc.ong
wget $url/floodfill.png
wget $url/player.png
